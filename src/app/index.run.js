(function() {
  'use strict';

  angular
    .module('retoLanding')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
